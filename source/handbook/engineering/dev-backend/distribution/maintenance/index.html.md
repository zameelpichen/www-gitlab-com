---
layout: markdown_page
title: "Distribution Team Infrastructure and Maintenance"
---

## On this page

- TOC
{:toc}

## Common links

* [Distribution Team Handbook](/handbook/engineering/dev-backend/distribution/)

## Infrastructure

As part of the team tasks, team has responsibility towards the following nodes:

* dev.gitlab.org: This internal GitLab instance runs nightly CE packages and is
  used for building official packages as well as hosting security release
  related MRs before publishing. Details of the node as well as the maintenance
  tasks can be found in [the dev.gitlab.org specific docs](./dev-gitlab-org.html).

* Build Machines: Runner manager machines that spins up machines that are used
  by various CI jobs for building and publishing packages. Details of the node
  as well as the maintenance tasks can be found in [the build machines specific docs](./build-machines.html)

* packages.gitlab.com : This is a package server that is used by Distribution
  team to ship GitLab CE and EE omnibus-gitlab packages, and by Verify team to
  ship gitlab-runner packages to the users.  GitLab CE and EE packages are built
  via our CI pipeline on `dev.gitlab.org`.

  Distribution uses the package server as a tool and doesn't have any
  maintenance tasks associated with it. Package server is currently deployed on
  our own infrastructure, from an omnibus type package. In case Production team
  requires help the team should do a best effort to help trough any issues.
